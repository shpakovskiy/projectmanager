@if (count($activity->changes['after']) == 1)
    {{ $activity->user->name }} обновил {{ key($activity->changes['after']) }} of the project
@else
    {{ $activity->user->name }} обновил проект
@endif
