<?php

namespace Tests\Unit;

use App\User;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_projects()
    {
        $user = factory('App\User')->create();
        $this->assertInstanceOf(Collection::class, $user->projects);
    }

    /** @test */
    function a_user_has_accessible_projects()
    {
        $vasya = $this->signIn();
        ProjectFactory::ownedBy($vasya)->create();
        $this->assertCount(1, $vasya->accessibleProjects());
        $petya = factory(User::class)->create();
        $katya = factory(User::class)->create();
        $project = tap(ProjectFactory::ownedBy($petya)->create())->invite($katya);
        $this->assertCount(1, $vasya->accessibleProjects());
        $project->invite($vasya);
        $this->assertCount(2, $vasya->accessibleProjects());
    }
}
